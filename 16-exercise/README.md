# Установка приложений

В этом занятии мы научимся работать со службами и читать системный журнал.\
В инструкции [Установка приложений](../16-solution/README.md) представлена шпаргалки по командам.\
Все задания выполняются на удалённом сервере\
Доступ к которому мы отработали в первом занятии [Подключение к удаленному серверу](../01-exercise/README.md)

## Задание

1. Подключитесь под своим пользователем на удалённый сервер
2. Найдите последнюю актуальную версию Java с помощью пакетного менеджера вашей системы
3. Установите Java с помощью пакетного менеджера
4. Обновите Java с помощью пакетного менеджера
5. Удалите Java с помощью пакетного менеджера