# Установка приложений

## Пакетные менеджеры

В Linux есть аналог AppStore и GooglePlay\
Они называются *Пакетные менеджеры*\
Самыми распространенными являются:

- `Advanced Packaging Tool` (`apt`) для семейства Debian
- `Yellowdog Updater, Modified` (`yum`) для семейства Red Hat

Есть менее популярные, например `Snappy` от Ubuntu или `Pacman` от Arch Linux

## Advanced Packaging Tool - apt

### Обновление базы данных о пакетах

```shell
sudo apt update
```

### Установить и обновление

```shell
sudo apt install openjdk-17-jdk
```

### Удаление пакета и конфигурационных файлов

```shell
sudo apt purge openjdk-17-jdk
```

### Удаление только пакета

```shell
sudo apt remove openjdk-17-jdk
```

### Список установленных приложений

```shell
apt list
```

### Информация о пакете

```shell
apt show openjdk-17-jdk
```

### Поиск пакета

```shell
apt search openjdk
```


## Yellowdog Updater, Modified - yum


### Список установленных пакетов

```shell
yum list installed
```

### Информация о пакете

### Установка

```shell
sudo yum install java-17-openjdk
```

### Обновление

```shell
sudo yum update java-17-openjdk
```

#### Обновить все пакеты

```shell
sudo yum update
```

### Удалить

```shell
sudo yum remove java-17-openjdk
```

### Информация о пакете

```shell
yum info java-17-openjdk
```

### Поиск пакета

```shell
yum search openjdk
```

### История изменений

```shell
yum history list
```

## Snappy

### Установка

```shell
sudo snap install openjdk
```

### Обновление

```shell
sudo snap refresh openjdk
```

#### Обновить все пакеты

```shell
sudo snap refresh
```

### Откатить

```shell
sudo snap revert openjdk
```

### Удалить

```shell
sudo snap remove openjdk
```

### Список установленных пакетов

```shell
snap list
```

### Информация о пакете

```shell
snap info openjdk
```

### История изменений

```shell
snap changes
```