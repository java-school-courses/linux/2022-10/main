# Навигация по системе

В этом занятии мы научимся базовым командам навигации.\
В инструкции [Навигация по системе](../02-solution/README.md) представлена шпаргалки по командам.\
Все задания выполняются на удалённом сервере\
Доступ к которому мы отработали в первом занятии [Подключение к удаленному серверу](../01-exercise/README.md)

## Задание

1. Подключитесь под своим пользователем на удалённый сервер
2. Создать каталог на удалённом сервере `~/LAST_NAME-FIRST_NAME`\
   Где `LAST_NAME` - ваша фамилия на латинице, `FIRST_NAME` - ваше имя на латинице
3. Создайте файл `text.txt`
4. Создайте вложенные каталоги `~/LAST_NAME-FIRST_NAME/dir1/dir2/dir3`
5. Удалите файл `text.txt`
6. Удалите каталог `LAST_NAME-FIRST_NAME`
7. Посмотрите файлы в корневом каталоге `/`
8. Зайдите в свою домашнюю директорию
9. Зайдите в домашнюю директорию соседа
10. Отключитесь от удалённого сервера