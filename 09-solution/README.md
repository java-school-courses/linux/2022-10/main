# Псевдонимы (alias)

Подобно другим интерпретатором в `shell` можно создавать свои команды на основе уже имеющихся команд

## Посмотреть список псевдонимов

```shell
alias
```

```shell
type ALIAS
```

## Создать псевдоним

```shell
alias NAME='COMMAND'
```

```shell
alias lah='ls -lah'
```

## Удалить псевдоним

```shell
unalias NAME
```

```shell
unalias lah
```