# Особенности файловой системы и консоли

## Все есть файл

- Обычный файл - файл
- Каталоги - файлы
- Ссылки на файл - файл
- Внешние устройства - файлы
- Информация о ресурсах - файлы
- и т.д.

### Время файла

- Создание
- Модификация
- Изменение

## Разделитель - пробел или табуляция

```shell
user@myhost:~$ mkdir name with space
user@myhost:~$ ls -l
итого 12
drwxrwxr-x 2 user group 4096 ноя  3 15:06  name
drwxrwxr-x 2 user group 4096 ноя  3 15:06  space
drwxrwxr-x 2 user group 4096 ноя  3 15:06  with
```

```shell
user@myhost:~$ mkdir "name with space"
user@myhost:~$ ls -l
итого 4
drwxrwxr-x 2 user group 4096 ноя  3 15:07 'name with space'
```

```shell
user@myhost:~$ mkdir name\ with\ space
user@myhost:~$ ls -l
итого 4
drwxrwxr-x 2 user group 4096 ноя  3 15:08 'name with space'
```

## Абсолютные и относительные пути

### Абсолютные пути

```shell
/home/user/work
```

```shell
~/work
```

```shell
${JAVA_HOME}/bin
```

### Относительные пути

```shell
work
```

```shell
dir1/dir2/dir3
```

## Аргументы командной строки

### Аргументы - флаги

#### Полная запись

```shell
ls --all
```

```shell
ls --all --human-readable --recursive
```

#### Короткая запись

```shell
ls -a
```

```shell
ls -a -h -R
```

```shell
ls -ahR
```

### Аргументы с параметрами

#### Полная запись

```shell
cp --target-directory=/home/user/dir/ file*
```

```shell
cp --target-directory /home/user/dir/ file*
```

#### Короткая запись

```shell
cp -t /home/user/dir/ file*
```

## Скрытые файлы

Все файлы и каталоги, которые начинаются с `.`

## Ссылки

В Linux, как и в Windows есть ярлыки\
Только здесь они называются ссылками\
Причём ссылки бывают двух типов:

- Мягкие
- Жёсткие

### Мягкие ссылки (Soft)

Близкий аналог ярлыков в Windows.\
Отдельный файл, со своими правами.\
Более новые технология, используется чаще.

#### Создать

```shell
ln -s SOURCE DESTINATION
```

```shell
ln -s file.txt soft-link-file.txt
```

### Жёсткие ссылки (Hard)

С точки зрения файловой системы - тот же самый файл, но с другим именем (расположением).\
Исторически - первый вариант ссылок в Unix.\
Используется редко.

#### Создать

```shell
ln SOURCE DESTINATION
```

```shell
ln file.txt hard-link-file.txt
```