# Ресурсы

## Оперативная память

```shell
free
```

### Размер памяти, удобный для человека (Мегабайты, Гигабайты и т.д.)

```shell
free --human
```

```shell
free -h
```

### Объяснение

```shell
user@myhost:~$ free -h
              total        used        free      shared  buff/cache   available
Mem:           15Gi       244Mi        13Gi       2.0Mi       2.1Gi        15Gi
Swap:            0B          0B          0B
```

```shell
              total        used        free      shared  buff/cache   available
Mem:           15Gi       244Mi        13Gi       2.0Mi       2.1Gi        15Gi
              [---]                                                   [--------]
                |                                                         |       
                |                                                         +-------> Свободная память                    
                +-----------------------------------------------------------------> Общий размер памяти                  
```

## Монитор ресурсов

```shell
top
```

### Вывод `top`

```
top - 22:12:03 up 21 days,  9:26,  7 users,  load average: 1.15, 1.11, 1.06
Tasks: 137 total,   1 running, 136 sleeping,   0 stopped,   0 zombie
%Cpu(s):  1.1 us,  0.4 sy,  0.0 ni, 98.5 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
MiB Mem :  16008.8 total,  13563.0 free,    244.9 used,   2200.9 buff/cache
MiB Swap:      0.0 total,      0.0 free,      0.0 used.  15423.0 avail Mem 

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND   
      1 root      20   0  167596  11556   8240 S   0.0   0.1   0:31.98 systemd   
      2 root      20   0       0      0      0 S   0.0   0.0   0:00.09 kthreadd  
      3 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 rcu_gp    
      4 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 rcu_par_gp
```

[Объяснение, как работать с `top`](top/README.md)

### Удобный аналог `top`

```shell
htop
```

## Информация о процессах

```shell
ps
```

### Все процессы, кроме фоновых

```shell
ps -a
```

### Выбирать процессы по имени команды

```shell
ps | grep java
```

### Показать PID по имени команды

```shell
pgrep java
```

## Тестируем время выполнения

```shell
time COMMAND
```

### Пример

```shell
user@myhost:~$ time sleep 2
real	0m2.001s
user	0m0.001s
sys	0m0.000s
```

## Постоянная память (жёсткий диск)

```shell
df
```

### Размер памяти, удобный для человека

```shell
df -h
```

### Информация о "фиктивных" файловых системах

```shell
df -a
```

### Информация о конкретном разделе

```shell
df PATH
```

```shell
df /home
```

## Узнать размер файла или каталога

```shell
du PATH
```

### Пример

```shell
du .
```

```shell
du /
```

### Размер памяти, удобный для человека

```shell
du -h /
```

## Список открытых файлов

```shell
lsof
```

### Файлы, открытые конкретным пользователем

```shell
lsof -u ${USER}
```

### Исключить конкретного пользователя

```shell
lsof -u^${USER}
```

### Показать ID процесса (PID), открывшего файл

```shell
lsof -t
```

### Файлы, связанные с сетью

#### Список всех сетевых подключений

```shell
lsof -i
```

#### Список файлов, открытых на конкретном порту

```shell
lsof -i TCP:22
```

#### Список файлов, открытых по диапазону портов

```shell
lsof -i TCP:1-1024
```

#### Список только IPv4 и IPv6 открытых файлов

```shell
lsof -i 4
```

```shell
lsof -i 6
```

## Информация о сети

```shell
netstat
```

## Все открытые порты

```shell
netstat --listening
```

```shell
netstat -l
```

## Отфильтровать TCP соединения

```shell
netstat --tcp
```

```shell
netstat -t
```

## Отфильтровать UDP соединения

```shell
netstat --udp
```

```shell
netstat -u
```

### Статистика по каждому из протоколов

```shell
netstat --statistics
```

```shell
netstat -s
```

### Показать список сетевых интерфейсов

```shell
netstat --interfaces
```

```shell
netstat -i
```

### Показать PID процесса, открывшего порт

Чтобы посмотреть PID процесса, который открывал другой пользователь *требуются привилегии супер пользователя*

```shell
netstat --program
```

```shell
netstat -p
```

### Вывести цифровой идентификатор, вместо того, чтобы подставлять текстовое значение

```shell
netstat --numeric
```

```shell
netstat -n
```

#### Отображение конкретные данные в цифровом виде

```shell
netstat --numeric-hosts
```

```shell
netstat --numeric-ports
```

```shell
netstat --numeric-users
```

### Примеры

#### Показать открытые TCP соединения

```shell
netstat -lt
```

#### Показать открытые TCP и UDP соединения

```shell
netstat -lt
```

#### Показать открытые TCP соединения с цифровыми значениями

```shell
netstat -ltn
```

#### Показать открытые TCP соединения с цифровыми значениями и PID

```shell
sudo netstat -ltnp
```

## Завершить процесс

```shell
kill PROCESS_ID
```

### Попросить процесс завершиться

```shell
kill 123
```

### Жёсткое, безальтернативное завершение процесса

```shell
kill -9 123
```

### Информация о всех видах сигналов

```shell
kill -l
```

## Выключить компьютер

```shell
sudo shutdown -h TIME
```

### Выключить сейчас

```shell
sudo shutdown -h now
```

### Выключить в 10:00

```shell
sudo shutdown -h 10:00
```

### Выключить через 5 минут

```shell
sudo shutdown -h +5
```

### Записать сообщение в системный журнал

```shell
sudo shutdown -h now "Hardware upgrade"
```

### Отменить запланированное выключение

```shell
sudo shutdown -c
```

## Перезагрузка системы

```shell
sudo reboot
```

```shell
sudo shutdown -r now
```

```shell
sudo systemctl reboot
```

## Узнать, кто залогирован в системе

```shell
who
```