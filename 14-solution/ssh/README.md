# SSH

`SSH` расшифровывается как *Secure Shell*.\
Служит для управления удалённым компьютером.

## Подключение к удалённому серверу

```shell
ssh USER_NAME@HOST
```

```shell
ssh user@localhost
```

*Если имена пользователей на клиенте и сервере совпадают, то логин можно не указывать*

## Скопировать свой ssh ключ на удалённый сервер

```shell
ssh-copy-id USER_NAME@HOST
```

### Скопировать default ключ

По-умолчанию передается открытая часть ключа `~/.ssh/id_rsa`

```shell
ssh user@localhost
```

### Скопировать выбранный ключ

```shell
ssh -i ~/.ssh/custom_id_rsa user@localhost
```

### Ручное добавление ключа на удалённый сервер

*Команда проводится на удалённом сервере*

#### Подготовка каталога `.ssh`

```shell
mkdir -p -m 700 ~/.ssh/
touch ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
```

#### Добавление ключа

```shell
cat id_rsa.pub >> ~/.ssh/authorized_keys
```

## Alias подключения

ssh клиент позволяет объединить группу настроек в один alias.\
Например две команды будут равносильны:

```shell
ssh my-server
```

```shell
ssh -i ~/.ssh/custom_id_rsa 
  -p 9900 \
  -o ServerAliveInterval=60 \
  -o ServerAliveCountMax=10 \
  user@example.com 
```

### Конфигурационный файл

В файле `~/.ssh/config` клиента необходимо добавить настройки типа

```text
KEY VALUE
```

Две обязательные настройки:

- `Host`
- `HostName`

### Пример настроек

```text
Host my-server
HostName example.com
User user
Port 9900
IdentityFile ~/.ssh/custom_id_rsa
ServerAliveInterval 60
ServerAliveCountMax 10
```

### Настройки `~/.ssh/config`

| Имя | По-умолчанию| Смысл |
|:----:|:----:|-------|
| Host                |      | Имя alias       |
| HostName            |      | IP или доменное имя, на которое соединяться       |
| User                |      |Логин пользователя на удалённом сервере       |
| Port                | 22   |Порт       |
| IdentityFile        | `~/.ssh/id_rsa` | Пусть на клиентской машине к ключу авторизации |
| IdentitiesOnly      | `no` | Запрет авторизации по паролю       |
| ServerAliveInterval | 0    | Если сервер недоступен, подождать этот промежуток времени и отправить ему сигнал вернуться в работу |
| ServerAliveCountMax |      | Максимальное число сообщений серверу, если он не отвечает |
| Compression         | `no` | Сжимать трафик?         |
| ForwardX11          | `no` | Включить возможность работать с GUI на удалённом сервере         |
| LocalForward        |      | Туннелирование - то есть перенаправление порта.          |
|                     |      | Пример: Вызываете `curl localhost:8080` на локальном компьютере, а на самом деле вызывается `localhost:8080` на удалённом |

## Выполнение одиночной команды по SSH

```shell
ssh USER_NAME@HOST COMMAND
```

### Пример

```shell
ssh my-server free -h
```

```shell
ssh my-server df -h
```

```shell
ssh my-server sudo reboot
```

## Отправка файлов

```shell
scp SOURCE DESTINATION
```

### Отправка файлов на сервер

```shell
scp local/path/file.txt my-server:/home/user/
```

### Отправка файлов на клиент

```shell
scp my-server:/var/log/message message.log
```