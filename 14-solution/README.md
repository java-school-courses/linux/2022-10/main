# Сеть

## Выполнить сетевой запрос

```shell
curl URL
```

### Пример

```shell
curl https://example.com
```

### Указать HTTP метод

```shell
curl --request HEAD https://example.com
```

```shell
curl -X GET https://example.com
```

### Передать тело сообщения

```shell
curl --data "{'key':'value'}" https://example.com
```

```shell
curl -d "{'key':'value'}" https://example.com
```

```shell
curl -d @file.txt https://example.com
```

### Передать HTTP заголовок

```shell
curl --header "X-First-Name: Joe" https://example.com
```

```shell
curl -H "X-First-Name: Joe" https://example.com
```

### Выводить HTTP заголовки ответа

```shell
curl --include https://example.com
```

```shell
curl -i https://example.com
```

### Сохранить вывод в файл

```shell
curl --output response.json https://example.com
```

```shell
curl -o response.json https://example.com
```

### Игнорировать сертификат сервера

```shell
curl --insecure https://example.com
```

```shell
curl -k https://example.com
```

### Подключить `ca` сертификат

```shell
curl --cacert CA-file.crt https://example.com
```

### Подключить открытый и закрытые ключи

```shell
curl --cert public.pem --key private.key https://example.com
```

### Авторизация по логину и паролю

```shell
curl --user user:secret https://example.com
```

```shell
curl -u user:secret https://example.com
```

### Расширенная информация вывода

```shell
curl --verbose https://example.com
```

```shell
curl -v https://example.com
```

## Узнать информацию о сетевых настройках

```shell
ip OBJECT
```

### Узнать IP адрес

```shell
ip address
```

```shell
ip addr
```

```shell
ip a
```

#### Список IP4 адресов

```shell
ip -4 addr
```

#### Список IP6 адресов

```shell
ip -6 addr
```

### Узнать о сетевых интерфейсах

```shell
ip link
```

```shell
ip l
```

### Показать таблицу маршрутов IP

```shell
ip route
```

## Проверка доступности сервера

```shell
ping HOST
```

### Пример

```shell
ping example.com
```

### Отправить 3 пакета

```shell
ping -c 3 example.com
```

### Отправлять пакеты раз в пол секунды

```shell
ping -i 0.5 example.com
```

### Отправлять пакеты по 1024 байт

```shell
ping -s 1024 example.com
```

### DoS атака на сервер

Посылать пакеты без ожидания возвращения.\
Так как это опасная операция, посылать пакеты без задержек может только привилегированный пользователь.

```shell
sudo ping -f example.com
```

```shell
ping -f -i 10 example.com
```

## Проверка доступности порта

```shell
nc -z HOST PORT
```

```shell
nc -z localhost 8080
```

### Написать статус проверки

```shell
nc -z -v HOST PORT
```

```shell
nc -zv localhost 8080
```

### Старый способ

```shell
telnet HOST PORT
```

```shell
telnet localhost 8080
```

#### Прослушать диапазон портов

```shell
nc -z example.com 20-80
```

## Маршрут пакета

```shell
traceroute HOST
```

```shell
traceroute example.com
```

## Узнать IP адрес сервера по host имени

```shell
host HOST
```

```shell
host example.com
```

## Удалённый доступ к компьютеру

### [Удалённый доступ к компьютеру по SSH](ssh/README.md)

## Синхронизация с удалённым сервером

```shell
rsync SOURCE DESTINATION
```

```shell
rsync code/ user@example.com:/home/user/code
```