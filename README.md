# Школа Java Middle Developer - Linux

## Ссылки

- [Виртуальная школа](https://sberuniversity.online/programs/19086/about)
- [Домашнее задание](https://gitlab.com/java-school-courses/linux/2022-10/main/-/tree/main/homework)
- [Jazz](https://jazz.sber.ru/a44pzm?psw=OBkAXQQODRxfCQdLWxoaD1ADGg)
- [Telegram](https://t.me/+vsrNgJcWV201OWVi)
- [Ведомость](https://docs.google.com/spreadsheets/d/1CThnsPAG3RazqcPj74bULsmHFRv6Do9GMIU6gK1u6gs)
- Linux Server: 188.72.109.30

## Содержание

### Подключение к удаленному серверу

[Учебник](01-solution/README.md)

[Задание](01-exercise/README.md)

### Навигация по системе

[Учебник](02-solution/README.md)

[Задание](02-exercise/README.md)

### Права доступа

[Учебник](03-solution/README.md)

[Задание](03-exercise/README.md)

#### Подробное описание прав доступа

[Учебник](03-solution/Подробное_описание/README.md)

### Справка о командах

[Учебник](04-solution/README.md)

[Задание](04-exercise/README.md)

### Основные команды

[Учебник](05-solution/README.md)

[Задание](05-exercise/README.md)

### Особенности файловой системы и консоли

[Учебник](06-solution/README.md)

[Задание](06-exercise/README.md)

### Интерпретатор командной строки

[Учебник](07-solution/README.md)

[Задание](07-exercise/README.md)

### Переменные окружения

[Учебник](08-solution/README.md)

[Задание](08-exercise/README.md)

### Псевдонимы (alias)

[Учебник](09-solution/README.md)

[Задание](09-exercise/README.md)

### Расширенная подстановка переменных окружения

[Учебник](10-solution/README.md)

[Задание](10-exercise/README.md)

### Структура файловой системы

[Учебник](11-solution/README.md)

[Задание](11-exercise/README.md)

### Ресурсы

[Учебник](12-solution/README.md)

[Задание](12-exercise/README.md)

#### `top` - Монитор ресурсов

[Учебник](12-solution/top/README.md)

### Работа с текстом

[Учебник](13-solution/README.md)

[Задание](13-exercise/README.md)

#### Текстовый редактор

[Учебник](13-solution/editor/README.md)

#### Сортировка

[Учебник](13-solution/sort/README.md)

### Сеть

[Учебник](14-solution/README.md)

[Задание](14-exercise/README.md)

#### SSH

[Учебник](14-solution/ssh/README.md)

### Управление пользователями

[Учебник](15-solution/README.md)

[Задание](15-exercise/README.md)

### Установка приложений

[Учебник](16-solution/README.md)

[Задание](16-exercise/README.md)

### bash-скрипты

[Учебник](17-solution/README.md)

[Задание](17-exercise/README.md)

#### Пример приложения

[Переводит текст в верхний или нижний регистр](17-solution/example/case.sh)

### Службы

[Учебник](18-solution/README.md)

[Задание](18-exercise/README.md)

### Планировщик задач

[Учебник](19-solution/README.md)

[Задание](19-exercise/README.md)

### Полезные команды

[Учебник](20-solution/README.md)

[Задание](20-exercise/README.md)

### Домашние задания

[Домашние задания](homework/README.md)

## Преподаватель

[Новый GitHub](https://github.com/ai-lenok) \
[Заблокированный GitHub](https://github.com/dzx912) \
[YouTube](https://www.youtube.com/watch?v=El5VZ8_ct6Y&list=PLE-xeQlBR2lnHrCVVte39YJfqjSimBUnT)