# Расширенная подстановка переменных окружения

## Задать значение по-умолчанию

|Команда| Если переменная задана| Если переменная НЕ задана| Переменная изменяется?| Статус выполнения|
|:------------------------------------------------:|---------|---------|:--------:|:----------:|
| ${PARAMETER} | Подставляет переменную | Подставляет null, то есть пустоту | Нет | 0 |
| ${PARAMETER:-DEFAULT} | Подставляет переменную | Подставляет DEFAULT значение | Нет | 0 |
| ${PARAMETER:=DEFAULT} | Подставляет переменную | Присваивает переменной DEFAULT значение | Да | 0 |
| ${PARAMETER:?TEXT_ERROR} | Подставляет переменную | Выводит TEXT_ERROR и возвращает статус 1 | Нет | 1 |
| ${PARAMETER:+MESSAGE} | Выводит MESSAGE | Подставляет null, то есть пустоту | Нет | 0 |

### ${PARAMETER}

```shell
user@myhost:~$ echo ${my_var}

user@myhost:~$ echo "status: $?"
status: 0
user@myhost:~$ my_var=10
user@myhost:~$ echo ${my_var}
10
user@myhost:~$ sleep ${delay}
usage: sleep seconds
```

### ${PARAMETER:-DEFAULT}

```shell
user@myhost:~$ echo ${my_var:-default}
default
user@myhost:~$ echo "status: $?"
status: 0
user@myhost:~$ echo ${my_var}

user@myhost:~$ my_var=10
user@myhost:~$ echo ${my_var:-default}
10
user@myhost:~$ sleep ${delay:-5}
```

### ${PARAMETER:=DEFAULT}

```shell
user@myhost:~$ echo ${my_var:=default}
default
user@myhost:~$ echo "status: $?"
status: 0
user@myhost:~$ echo ${my_var}
default
user@myhost:~$ my_var=10
user@myhost:~$ echo ${my_var:=default}
10
user@myhost:~$ sleep ${delay:=5}
```

### ${PARAMETER:?DEFAULT}

```shell
user@myhost:~$ echo ${my_var:?not set}
bash: my_var: not set
user@myhost:~$ echo "status: $?"
status: 1
user@myhost:~$ echo ${my_var}

user@myhost:~$ my_var=10
user@myhost:~$ echo ${my_var:?not set}
10
user@myhost:~$ sleep ${delay:?not set}
bash: delay: not set
```

### ${PARAMETER:+DEFAULT}

```shell
user@myhost:~$ echo ${my_var:+is set}

user@myhost:~$ echo "status: $?"
status: 0
user@myhost:~$ echo ${my_var}

user@myhost:~$ my_var=10
user@myhost:~$ echo ${my_var:+is set}
is set
user@myhost:~$ sleep ${delay:+5}
usage: sleep seconds
```

## Работа с подстроками

В современных `shell` есть операторы работы с подстроками, явно позаимствованные из таких интерпретаторов, как Python и
Ruby.

Пример из документации

```shell
$ string=01234567890abcdefgh
$ echo ${string:7}
7890abcdefgh
$ echo ${string:7:0}

$ echo ${string:7:2}
78
$ echo ${string:7:-2}
7890abcdef
$ echo ${string: -7}
bcdefgh
$ echo ${string: -7:0}

$ echo ${string: -7:2}
bc
$ echo ${string: -7:-2}
bcdef
```