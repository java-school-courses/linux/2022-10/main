# Управление пользователями

## Посмотреть список пользователей

Список всех пользователей изначально хранится в файле `/etc/passwd`

```shell
less /etc/passwd
```

Раньше этот файл хранил пароли, теперь пароли перенесены в файл `/etc/shadow`\
И в `/etc/passwd` хранятся только пользователи

### Пример содержания `/etc/passwd`

```
user:x:1000:1000:Regular user:/home/user:/bin/bash
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
```

### Объяснение полей

| № | Имя                           | Пример         | Объяснение                                         |
|:-:|-------------------------------|----------------|----------------------------------------------------|
| 1 | Имя пользователя              | root           |                                                    |
| 2 | Пароль                        | x              | `x` обозначает, что пароль хранится в другом месте |
| 3 | User ID                       | 1000           |                                                    |
| 4 | Group ID                      | 1000           |                                                    |
| 5 | Комментарий                   | Rpcbind Daemon | Небольшое пояснение для системного администратора  |
| 6 | Домашний каталог пользователя | /home/user     |                                                    |
| 7 | Shell                         | /bin/bash      | Пусть к интерпретатору командной строки            |
|   |                               |                | в который попадает пользователь при авторизации    |

## Создать пользователя

```shell
sudo useradd LOGIN
```

```shell
sudo useradd user
```

## Модифицировать пользователя

```shell
sudo usermod [OPTIONS...] LOGIN
```

### Добавить пользователя в дополнительную группу

```shell
sudo usermod --append --groups GROUPS... LOGIN
```

```shell
sudo usermod LOGIN -a -G GROUPS... LOGIN
```

```shell
sudo usermod \
--append \
  --groups prometheus docker \
user
```

### Заблокировать пароль пользователя

```shell
sudo usermod --lock user
```

```shell
sudo usermod -L user
```

### Разблокировать пароль пользователя

```shell
sudo usermod --unlock user
```

```shell
sudo usermod -U user
```

## Задать пароль пользователю

### Поменять свой пароль

```shell
passwd
```

### Поменять пароль другому пользователю

```shell
sudo passwd LOGIN
```

```shell
sudo passwd another_user
```

### Задать количество дней жизни пароля

```shell
passwd --maxdays MAX_DAYS
```

```shell
passwd -x MAX_DAYS
```

```shell
passwd --maxdays 70
```

## Удалить пользователя

```shell
sudo userdel LOGIN
```

```shell
sudo userdel user
```

### Удалить пользователя вместе с домашней директорией

```shell
sudo userdel --remove user
```

```shell
sudo userdel -r user
```

### Удалить пользователя даже если он ещё залогирован

```shell
sudo userdel --force user
```

```shell
sudo userdel -f user
```

## Блокировка пользователя после нескольких неудачных попыток входа

На практике встречается утилита `pam_tally2`\
Поэтому рассмотрим как с ней работать

### Посмотреть статистику неуспешных входов

```shell
sudo pam_tally2
```

### Посмотреть статистику неуспешных входов по конкретному пользователю

```shell
sudo pam_tally2 --user USER
```

```shell
sudo pam_tally2 -u USER
```

```shell
sudo pam_tally2 --user user
```

### Разблокировать пользователя

```shell
sudo pam_tally2 --user user --reset
```

```shell
sudo pam_tally2 -u user -r
```