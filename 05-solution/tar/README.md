# Работа с архивом

## Tar

```shell
tar OPTIONS... ARHIVE_NAME FILE...
```

### Не сжатый архив

#### Собрать

```shell
tar -cf file.tar *.txt
```

```shell
tar cf file.tar *.txt
```

##### Показать список файлов

```shell
tar -cfv file.tar *.txt
```

#### Вынуть

```shell
tar -xf file.tar
```

```shell
tar xf file.tar
```

##### Показать список файлов

```shell
tar -xfv file.tar
```

### Сжатый архив

#### Собрать

```shell
tar -cfz file.tar.gz *.txt
```

#### Вынуть

```shell
tar -xfz file.tar.gz
```

### Добавить файл в архив

```shell
tar -rf file.tar new_file.txt
```

### Показать содержимое архива

```shell
tar -tf file.tar.gz
```

## Zip

```shell
zip ARHIVE_NAME FILE...
```

```shell
unzip ARHIVE_NAME
```

### Пример

#### Создать архив

```shell
zip files.zip *.txt
```

#### Распаковать архив

```shell
unzip files.zip
```

### Сжать каталог

```shell
zip -r dir.zip dir/
```

### Выбрать степень сжатия

#### Без сжатия

```shell
zip -0 files.zip *.txt
```

#### Максимальное сжатие

```shell
zip -9 files.zip *.txt
```

### Добавить файл в архив

```shell
zip –u filename.zip new_file.txt
```

### Удалить оригинальный файл после сжатия

```shell
zip –m filename.zip goodbye_file.txt
```