# Основные команды

|       Команда       | Ссылка                                                        |
|:-------------------:|---------------------------------------------------------------|
|     `ls` `stat`     | [Показать файлы и директории в каталоге](ls/README.md)        |
|        `cp`         | [Скопировать файл или каталог](cp/README.md)                  |
|        `mv`         | [Переименовать/переместить файл или каталог](mv/README.md)    |
|       `mkdir`       | [Создание каталога](mkdir/README.md)                          |
|        `rm`         | [Удаление файла или каталога](rm/README.md)                   |
|       `touch`       | [Обновить время доступа и модификации файла](touch/README.md) |
|       `file`        | [Определить тип файла](file/README.md)                        |
|        `cat`        | [Вывод текста в стандартный вывод](cat/README.md)             |
| `tar` `zip` `unzip` | [Работа с архивом](tar/README.md)                             |
|       `date`        | [Узнать текущую дату и время](date/README.md)                 |
|       `find`        | [Поиск файла](find/README.md)                                 |
|       `sleep`       | [Пауза](sleep/README.md)                                      |
|       `clear`       | [Очистка экрана](clear/README.md)                             |
|       `exit`        | [Выход](exit/README.md)                                       |
|        `env`        | [Показать переменные окружения](env/README.md)                |
|      `uptime`       | [Время работы системы](uptime/README.md)                      |
|        `dd`         | [Низкоуровневая запись на диск](dd/README.md)                 |
|        `mc`         | [Midnight Commander](mc/README.md)                            |
|       `tmux`        | [Tmux](tmux/README.md)                                        |