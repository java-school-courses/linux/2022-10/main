# Переименовать/переместить файл или каталог

```shell
mv SOURCE... DESTINATION
```

## Пример

```shell
mv source_file dest_file
```

После этого файл `source_file` перестанет существовать\
Если файл `dest_file` существует, его перезапишут

## Спросить перед затиранием

```shell
mv -i source_file dest_file
```

## Переместить несколько файлов в каталог

```shell
mv source_file1 source_file2 dest_dir/
```

## Переместить каталог

```shell
mv source_dir dest_dir
```

У этой команды есть две разные логики выполнения
- Если dest_dir **не существует**
`source_dir` переименуется в `dest_dir`
- Если dest_dir **существует**
  `source_dir` переместится внутрь `dest_dir`
