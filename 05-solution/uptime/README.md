# Время работы системы

```shell
uptime
```

## Вывести в удобном для человека виде

```shell
uptime --pretty
```

```shell
uptime -p
```

Не работает в MacOS