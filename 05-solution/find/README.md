# Поиск файла

```shell
find PATH OPTIONS...
```

## Поиск по имени

```shell
find / -name file.txt
```

## Поиск по шаблону имени

```shell
find / -name "*.txt"
```

## Поиск по имени без учёта регистра

```shell
find / -iname file.txt
```

## Поиск каталога

```shell
find / -type d
```

## Поиск по маске доступа

## Точнее соответствие прав

```shell
find / -perm 777
```

```shell
find / -perm 1777
```

```shell
find / -perm u=r
```

a = all = ugo = user, group, other

```shell
find / -perm a=x
```

## Пересечение прав

```shell
find / -perm /u=r
```

```shell
find / -perm /a=x
```

## Поиск файла по владельцу

```shell
find / -user root
```

## Поиск файла по группе владельца

```shell
find / -group developer
```

## Поиск по размеру файла

## Точный размер

Ровно 50 Мб

```shell
find / -size 50M
```

## Диапазон значений

50 Мб < Размер < 100 Мб

```shell
find / -size +50M -size -100M
```

## Поиск по времени (создание, модификация, доступ)

## По времени создания (Create)

```shell
find / -ctime 50h
```

## По времени модификации (Modify)

```shell
find / -mtime 50h
```

## По времени доступа (Access)

```shell
find / -atime 50h
```

## Формат времени

|Параметр|Размерность|
|:---:|---|
|s| Секунда |
|m| Минута (60 секунд)|
|h| Час (60 минут) |
|d| День (24 часа) |
|w| Неделя (7 дней) |

## Диапазон значений

50 дней < Модифицирован < 100 дней назад

```shell
find / -mtime +50d –mtime -100d
```

## Объединение разных опций

```shell
find / -name "*.txt" -size 50M -user root
```

## Выполнить операцию над найденными файлами

```shell
find / -name "*.txt" -exec COMMAND {} \;
```

## Вывод содержимого найденных файлов

```shell
find / -name "*.txt" -exec cat {} \;
```

## Удалить найденные директории

```shell
find / -name dir -type d -exec rm -r {} \;
```