# Низкоуровневая запись на диск

<span style="color:#D00000">**Опасная команда**</span>

```shell
dd OPTIONS...
```

## Скопировать весь диск `sda` в диск `sdb`

```shell
dd if=/dev/sda of=/dev/sdb
```

## Скопировать блоками по 4 килобайта

```shell
dd if=/dev/sda of=/dev/sdb bs=4096
```

```shell
dd if=/dev/sda of=/dev/sdb bs=4k
```

## Очистить (затереть, отформатировать) жесткий диск `sda`

```shell
dd if=/dev/zero of=/dev/sda
```