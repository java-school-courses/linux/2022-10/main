# Скопировать файл или каталог

```shell
cp SOURCE... DESTINATION
```

## Пример

```shell
cp source_file dest_file
```

Если файл `dest_file` существует, его перезапишут

## Спросить перед затиранием

```shell
cp -i source_file dest_file
```

## Скопировать несколько файлов в каталог

```shell
cp source_file1 source_file2 dest_dir/
```

## Скопировать по маске

```shell
cp *.txt dest_dir/
```

## Скопировать каталог внутрь другого каталога

```shell
cp -r source_dir dest_dir
```

## Скопировать каталог

```shell
cp -r source_dir/* dest_dir
```

## Вывести лог копирования

```shell
cp -r -v source_dir dest_dir
```

```shell
cp -rv source_dir dest_dir
```

Нестандартная опция, лучше не использовать в скриптах