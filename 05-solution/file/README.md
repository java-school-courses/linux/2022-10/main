# Определить тип файла

```shell
file FILE...
```

## Пример

```shell
file file.txt dir
```

```shell
file *
```

## Показать тип файла без его названия

```shell
file -b file.txt
```

## Показать тип файла внутри архива

```shell
file -z image.tar.gz
```