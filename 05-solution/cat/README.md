# Вывод текста в стандартный вывод

```shell
cat FILE...
```

## Пример

```shell
cat file.txt
```

## Вывести номер строки

```shell
cat -n file.txt
```

## Вывести строки в обратном порядке

```shell
tac file.txt
```