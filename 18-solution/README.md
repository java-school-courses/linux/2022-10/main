# Службы

## Systemd

Подсистема, которая занимается двумя функциями:

- Инициализация остальных инструментов операционной системы;
- Управление службами.

## Что такое службы

Программы, запущенные как фоновые процессы.\
Могут заниматься чем угодно:

- Сбор логов;
- Веб-сервер;
- Работа ssh;
- и т.д.

Существует следующие виды служб:

- service - обычные программы
- target - группа служб
- automount - точка автоматического монтирования
- device - файл устройства, генерируется на этапе загрузки
- mount - точка монтирования
- path - файл или папка
- scope - процесс
- slice - группа системных служб systemd
- snapshot - сохраненное состояние запущенных служб
- socket - сокет для взаимодействия между процессами.

Далее мы будем работать в основном с `service`.\
Но в начале, познакомимся как работать со службами.

## Работа со службами

Основное приложение для работы со службами - `systemctl`.\
У него много разных сценариев работы.\
Рассмотрим основные из них.

### Посмотреть службы всех видов

```shell
sudo systemctl list-units
```

### Посмотреть все сервисы

```shell
sudo systemctl list-units --type=service
```

### Посмотреть список служб в автозапуске

```shell
sudo systemctl list-units --state=enabled
```

## Работа с сервисами

```shell
sudo systemctl COMMAND UNIT...
```

Где `COMMAND` - команда, которую хотите выполнить.\
`UNIT` - сервис, над которыми выполняется операция.

Пример:

```shell
sudo systemctl start nginx.service
```

```shell
sudo systemctl start nginx
```

### Запустить сервис

```shell
sudo systemctl start nginx.service
```

### Остановить сервис

```shell
sudo systemctl stop nginx.service
```

### Перезапустить сервис

```shell
sudo systemctl restart nginx.service
```

### Посмотреть статус сервиса

```shell
sudo systemctl status nginx.service
```

### Проверить, что сервис активен

```shell
sudo systemctl is-active nginx.service
```

### Добавить сервис в автозапуск

```shell
sudo systemctl enable nginx.service
```

### Убрать сервис из автозапуска

```shell
sudo systemctl disable nginx.service
```

### Проверить, что сервис в автозапуске

```shell
sudo systemctl is-enabled nginx.service
```

### Статус сервиса

```shell
user@myhost:~$ sudo systemctl status sshd.service
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2022-11-15 15:44:04 MSK; 1 weeks 3 days ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 12609 (sshd)
      Tasks: 1 (limit: 19104)
     Memory: 39.9M
     CGroup: /system.slice/ssh.service
             └─12609 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
```

|  Секция  | Объяснение                             |
|:--------:|----------------------------------------|
|  Loaded  | Загружен ли сервис                     |
|          | Путь к файлу, который описывает сервис |
|          | Находится ли сервис в автозагрузке     |
|  Active  | Работает ли сервис и как долго         |
|   Docs   | Где искать документацию к сервису      |
| Main PID | Process ID                             |
|  Tasks   | Количество текущих процессов           |
|          | Максимальное число дочерних процессов  |
|  Memory  | Оперативная память                     |
|  CGroup  | Контрольные группы                     |

## Логи служб

Все логи, отправляемые в stdout и stderr `systemd` записывает в бинарные файлы и их можно посмотреть с помощью
утилиты `journalctl`

### Просмотр всего лога systemd

```shell
sudo journalctl
```

### Фильтрация логов по имени сервиса

```shell
sudo journalctl --unit UNIT
```

```shell
sudo journalctl -u UNIT
```

```shell
sudo journalctl --unit nginx.service
```

### Фильтрация логов по пути приложения

```shell
sudo journalctl PATH_TO_APP
```

```shell
sudo journalctl /usr/sbin/nginx
```

### Фильтрация логов по Process ID

```shell
journalctl _PID=1
```

### Фильтрация логов по User ID владельца

```shell
journalctl _UID=1000
```

### Фильтрация логов по Group ID владельца

```shell
journalctl _GID=1000
```

### Фильтрация логов по времени

#### since

```shell
sudo journalctl --since TIME
```

```shell
sudo journalctl --S TIME
```

#### until

```shell
sudo journalctl --until TIME
```

```shell
sudo journalctl -U TIME
```

#### Пример

```shell
journalctl --since "2022-12-31 01:43:15"
```

```shell
sudo journalctl --until yesterday
```

```shell
sudo journalctl --since 22:00 --until "1 hour ago"
```

#### Формат времени

```text
YYYY-MM-DD HH:MM:SS
```

Если какое-то поле не указано, то подставляется значение от текущего времени

##### Размерности времени

- usec, us, µs
- msec, ms
- seconds, second, sec, s
- minutes, minute, min, m
- hours, hour, hr, h
- days, day, d
- weeks, week, w
- months, month, M (определён как 30.44 days)
- years, year, y (определён как 365.25 days)

##### Псевдонимы

- now
- today
- yesterday
- tomorrow

##### Относительное время

Можно поставить в начало символ `+` или `-`, тогда дата сместится на выбранный период вперед или назад от текущего
момента.

Так же, можно поставить слово `ago` или `left` в конец и это будет смещение назад

Пример:

```text
+3h30min
-5s
11min ago
```

#### Unix time

Можно указать `@` и после неё количество секунд прошедших с 1 января 1970 года

##### Пример

Представим, что сейчас `2012-11-23 18:15:22`, и наш часовой пояс `UTC+8`

```text
  Fri 2012-11-23 11:12:13 → Fri 2012-11-23 11:12:13
      2012-11-23 11:12:13 → Fri 2012-11-23 11:12:13
  2012-11-23 11:12:13 UTC → Fri 2012-11-23 19:12:13
               2012-11-23 → Fri 2012-11-23 00:00:00
                 12-11-23 → Fri 2012-11-23 00:00:00
                 11:12:13 → Fri 2012-11-23 11:12:13
                    11:12 → Fri 2012-11-23 11:12:00
                      now → Fri 2012-11-23 18:15:22
                    today → Fri 2012-11-23 00:00:00
                today UTC → Fri 2012-11-23 16:00:00
                yesterday → Fri 2012-11-22 00:00:00
                 tomorrow → Fri 2012-11-24 00:00:00
tomorrow Pacific/Auckland → Thu 2012-11-23 19:00:00
                 +3h30min → Fri 2012-11-23 21:45:22
                      -5s → Fri 2012-11-23 18:15:17
                11min ago → Fri 2012-11-23 18:04:22
              @1395716396 → Tue 2014-03-25 03:59:56
```

### Фильтрация логов по уровню логирования

```shell
sudo journalctl --priority=LEVEL
```

```shell
sudo journalctl -p LEVEL
```

| № | Наименование | Расшифровка | Описание                                             |
|:-:|:------------:|:-----------:|------------------------------------------------------|
| 0 |    emerg     |  emergency  | Неработоспособность системы                          |
| 1 |    alert     |   alerts    | Предупреждения, требующие немедленного вмешательства |
| 2 |     crit     |  critical   | Критическое состояние                                |
| 3 |     err      |   errors    | Ошибки                                               |
| 4 |   warning    |   warning   | Предупреждения                                       |
| 5 |    notice    |   notice    | Уведомления                                          |
| 6 |     info     |    info     | Информационные сообщения                             |
| 7 |    debug     |    debug    | Отладочные сообщения                                 |

```shell
sudo journalctl --priority=2
```

```shell
sudo journalctl --priority=crit
```

### Фильтрация с помощью grep-like стиля

```shell
sudo journalctl --grep=PATTERN
```

```shell
sudo journalctl -g PATTERN
```

#### Пример

```shell
sudo journalctl --grep='reboot'
```

### Показывать новые появившиеся события

```shell
sudo journalctl --follow
```

```shell
sudo journalctl -f
```

### Перемотать журнал в конец

```shell
sudo journalctl --pager-end
```

```shell
sudo journalctl -e
```

### Показывать самые новые логи в начале, а самые старые в конце

```shell
sudo journalctl --reverse
```

```shell
sudo journalctl -r
```

### Ротация логов

```shell
sudo journalctl --vacuum-size=100M
```

```shell
sudo journalctl --vacuum-time=7d
```

```shell
sudo journalctl --vacuum-files=1
```

### Задать формат вывода

```shell
sudo journalctl --output=TYPE
```

```shell
sudo journalctl -o TYPE
```

#### Пример

```shell
sudo journalctl --output=json
```

#### Форматы

|        Имя        | Описание                                                            |
|:-----------------:|---------------------------------------------------------------------|
|       short       | syslog style                                                        |
|                   | Формат по-умолчанию                                                 |
|     short-iso     | syslog style. Время в формате ISO 8601                              |
|  short-monotonic  | syslog style. Время в формате                                       |
|   short-precise   | syslog style. Время с точностью до наносекунд                       |
|    short-unix     | syslog style. UNIX секунды отсчитываются от 1 января 1970 года      |
| short-iso-precise | syslog style. Время в формате ISO 8601 включая микросекунды         |
|    short-full     | syslog style. Время в формате `День недели` + `YYYY-MM-DD HH:MM:SS` |
|     with-unit     | Похож на `short-full`, но в названии службы добавляет её тип        |
|                   |                                                                     |
|        cat        | Отображает только сообщение, без дополнительных полей               |
|                   |                                                                     |
|       json        | Стандартный JSON в одну строку                                      |
|    json-pretty    | JSON, удобный для человека                                          |
|     json-sse      | JSON, обёрнутый в формат `data : {}` для Server-Sent Events         |
|     json-seq      | Однострочный JSON, с разделителями формата JSON Text Sequences      |
|                   |                                                                     |
|      export       | Journal Export Format, специально для экспорта логов                |
|      verbose      | Расширенный формат, показывающий скрытые поля                       |