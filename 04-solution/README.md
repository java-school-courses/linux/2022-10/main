# Справка о командах

## Справка приложения о себе

```shell
APP -h
```

```shell
APP --help
```

### Пример

```shell
java -h
```

```shell
java --help
```

## Документация о команде - Manual

```shell
man COMMAND
```

### Пример

```shell
man chmod
```

### Документация выбранной страницы

```shell
man 2 chmod
```

### Виды страниц man

1. `User commands` - как пользоваться программой
2. `System calls` - системные вызовы ядра Linux
3. `Library functions ` - функции стандартной библиотеки C
4. `Devices` - различных устройства, большинство из которых находится в /dev
5. `Files` - описывает различные форматы файлов и файловые системы и включает
6. `Overviews, conventions, etc` - обзоры, соглашения и прочее
7. `Superuser and system administration commands` - команды для системных администраторов

## info - альтернативная документация

```shell
info COMMAND
```

### Пример

```shell
info chmod
```

## Поиск документации по ключевым словам

```shell
whatis KEYWORD
```

### Пример

```shell
whatis chmod
```

### Поиск по регулярному выражению

```shell
whatis -r ^ch.*
```

### Поиск только определенных номеров страниц

```shell
whatis -s 1 chmod
```

```shell
whatis -s 1 -r ^ch.*
```

### Поиск связанных страниц

```shell
apropos KEYWORD...
```

## Найти местоположение программы

```shell
which APP
```

### Пример

```shell
which java
```

## Найти местоположение программы, документации, исходников

```shell
whereis APP
```

### Пример

```shell
whereis java
```

На самом деле `whereis` ищет

- Двоичные файлы
- Файлы исходного кода
- Файлы документации

### Поиск двоичного файла - приложения (binary)

```shell
whereis -b java
```

### Поиск документации (manual)

```shell
whereis -b java
```

### Что собой представляет команда по факту

```shell
type ls
```

```shell
type ll
```

## Информация о системе

```shell
uname --all
```

```shell
uname -a
```