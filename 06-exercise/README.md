# Особенности файловой системы и консоли

В этом занятии мы научимся работать со службами и читать системный журнал.\
В инструкции [Особенности файловой системы и консоли](../06-solution/README.md) представлена шпаргалки по командам.\
Все задания выполняются на удалённом сервере\
Доступ к которому мы отработали в первом занятии [Подключение к удаленному серверу](../01-exercise/README.md)

## Задание

1. Подключитесь под своим пользователем на удалённый сервер
2. Создайте файл с именем `name with space`
3. Создайте скрытый файл
4. Создайте обычный файл `file.txt`
5. Создайте мягкую ссылку на файл `file.txt`
6. Создайте жёсткую ссылку на файл `file.txt`