# Права доступа

## Посмотреть права доступа

```shell
ls -l
```

```shell
user@myhost:~$ touch file.txt
user@myhost:~$ ls -l file.txt
-rw-r--r--  1 user  group  0 20 окт 10:07 file.txt
|[-][-][-]    [--]  [---]
| |  |  |      |      |       
| |  |  |      |      +--------------> Группа                    
| |  |  |      +---------------------> Владелец                  
| |  |  +----------------------------> Права других пользователей 
| |  +-------------------------------> Права группы              
| +----------------------------------> Права владельца           
+------------------------------------> Тип файла
```

## Отобразить пользователя и его группы

```shell
id
```

```shell
user@myhost:~$ id
uid=1000(user) gid=1000(group) groups=1000(group)
```

## Отобразить текущего пользователя

```shell
whoami
```

## Назначить права

### Цифровой вид

```shell
chmod 640 file.txt
```

### Символьный вид

```shell
chmod u=rwx,g=rwx,o=rwx file.txt
```

```shell
chmod ugo=rwx file.txt
```

```shell
chmod u=rw,g=r file.txt
```

### Добавить права

```shell
chmod +rx file.txt
```

### Убрать права

```shell
chmod -x file.txt
```

## Сводная таблица прав

| Атрибут  | Файл                         | Каталог                                              |
|:--------:|------------------------------|------------------------------------------------------|   
|   Read   | Открывать и читать файл      | Читать содержимое каталога                           |
|  Write   | Записывать содержание файла  | Создавать, удалять, переименовывать файлы в каталоге |
| eXecute  | Выполнять файл как программу | Входить в каталог, выполнять `cd` для каталога       |

## Установка специальных прав: Setuid, Setgid, Sticky bit

### Setuid - запуск исполняемого файла как будто мы его владелец

Обычно применяется для root

```shell
chmod u+s file.txt
```

```shell
chmod 4640 file.txt
```

```shell
user@myhost:~$ ls -l /usr/bin/sudo
-rwsr-xr-x 1 root root 166056 Jan 19  2021 /usr/bin/sudo
```

### Setgid - запуск исполняемого файла как будто мы его в его группе

```shell
chmod g+s file.txt
```

```shell
chmod 2640 file.txt
```

```shell
user@myhost:~$ ls -l /usr/bin/crontab
-rwxr-sr-x 1 root crontab 43720 Feb 13  2020 /usr/bin/crontab
```

### Sticky bit - удалить файлы в каталоге может только владелец

```shell
chmod +t file.txt
```

```shell
chmod 1640 file.txt
```

```shell
user@myhost:~$ ls -ld /tmp
drwxrwxrwt 14 root root 4096 Oct 21 18:34 /tmp
```

## Задание владения файлом

```shell
user@myhost:~$ touch file.txt
user@myhost:~$ ls -l file.txt
-rw-r--r--  1 user  group  0 20 окт 10:07 file.txt
 [-][-][-]    [--]  [---]
               |      |       
               |      +--------------> Группа
               +---------------------> Владелец
```

```shell
user@myhost:~$ id
uid=1000(user) gid=1000(group) groups=1000(group)
```

### Owner = User

#### Символьный вид

```shell
chown user file.txt
```

#### Цифровой вид

```shell
chown 1000 file.txt
```

### Group

#### Символьный вид

```shell
chgrp group file.txt
```

```shell
chown :group file.txt
```

#### Цифровой вид

```shell
chown :1000 file.txt
```

### Задать одновременно пользователя и группу

#### Символьный вид

```shell
chown user:group file.txt
```

#### Цифровой вид

```shell
chown 1000:1000 file.txt
```

## Рекурсивное задание прав

То есть всех файлов и директорий внутри выбранного каталога

```shell
chown -R user:group file.txt
```

```shell
chgrp -R group file.txt
```

## Установить права файлов при создании

### Символьный вид

```shell
umask u=rw,go=r
```

### Цифровой вид

```shell
umask 644 file.txt
```

### Пример

```shell
user@myhost:~$ umask 644
user@myhost:~$ touch file1.txt
user@myhost:~$ ls -l file1.txt
-rw-r--r--  1 user  group  0 20 окт 10:07 file1.txt
user@myhost:~$ umask 666
user@myhost:~$ touch file2.txt
user@myhost:~$ ls -l file2.txt
-rw-rw-rw-  1 user  group  0 20 окт 10:10 file2.txt
```

### Добавить пользователя в группу

```shell
usermod -a -G group user
```

Более подробно рассмотрим в разделе [Управление пользователями](../15-solution/README.md)

## Смена текущего пользователя

### Зайти под другим пользователем

```shell
su user
```

#### Зайти под root

```shell
su
```

```shell
su -
```

```shell
su root
```

## Выполнить команду под другим пользователем

```shell
sudo -u user whoami
```

```shell
su user -c 'whoami'
```

### Выполнить под root

```shell
sudo whoami
```

```shell
sudo -u root whoami
```

## Редактирование прав sudo в системе

<span style="color:#D00000">Опасная команда</span>

```shell
sudo visudo
```

## SELinux

В дистрибутивах, основанных на Red hat используется расширенная система прав SELinux\
Мы не будем её рассматривать\
Но вам полезно знать, что она существует

Несколько важных вещей, которые нужны для понимания SELinux

- Эта система установлена в ядро Linux поверх классической системы прав
- SELinux позволяет ограничить приложения в правах Чтобы скрипт, который запустите от своего имени, не отправил ваши
  ssh-ключи злоумышленнику