# Интерпретатор командной строки

## Командная строка - это приложение

Командная строка - это приложение, которое входит в состав операционной системы.\
Современные интерпретаторы командной строки по функционалу сравнимы с такими интерпретаторами, как Ruby или Python.\
Но назначения у них разные.\
Основная задача интерпретатора командной строки - запуск других приложений.\
У интерпретатора Python - выполнение скриптов.

На английском языке интерпретатор командной строки называется `shell`.\
Далее будем использовать английский термин.

## Самые популярные shell'ы

| Название  |                       Ссылка                       | В каких ОС по-умолчанию  | Короткое описание                                             |
|:---------:|:--------------------------------------------------:|:------------------------:|---------------------------------------------------------------|
|    sh     | [wiki](https://ru.wikipedia.org/wiki/Bourne_shell) |      Unix version 7      | Оригинальный shell, разработанный в Unix. Стандарт де-факто.  |
|   bash    |   [GNU Bash](https://www.gnu.org/software/bash/)   |          Linux           | Самый популярный shell. Расширяет `sh`.                       |
|    zsh    |             [Zsh](https://www.zsh.org)             |          MacOS           | Второй по популярности shell, после `Bash`. Расширяет `Bash`. |

В этом курсе мы придерживаемся функциональности `Bash`.

### Прагины для shell

Если вам понравился `zsh`, то может заинтересовать его расширение [Oh My Zsh](https://ohmyz.sh).\
Это `zsh`, расширенный множеством плагинов.

## Запуск shell в shell'е

Так как `shell` - это приложение, а `shell` нужен, чтобы запускать приложения.\
То в `shell` можно запустить самого себя.\
Получается сон во сне, как в фильме "Inception" ("Начало")

## Инициализация

Когда `Bash` стартует, он запускает скрипты, располагающиеся по стандартным путям.\
Опишем логику запуска этих скриптов

|        Путь        | Условие запуска                 | Описание                                                                          |
|:------------------:|---------------------------------|-----------------------------------------------------------------------------------|
|   `/etc/profile`   | Запуск ОС                       | Общие настройки системы                                                           |
|                    |                                 |                                                                                   |
| `~/.bash_profile`  | Авторизация пользователя        | В начале проверяет этот файл, если он доступен, то завершаем инициализацию `Bash` |
|  `~/.bash_login`   | Авторизация пользователя        | Если нет `~/.bash_profile`, то проверяем этот файл                                |
|    `~/.profile`    | Авторизация пользователя        | Если нет `~/.bash_login`, то проверяем этот файл                                  |
|                    |                                 |                                                                                   |
| `~/.bash_logout `  | Выход                           |                                                                                   |
|                    |                                 |                                                                                   |
| `/etc/bash.bashrc` | Запуск нового терминала/вкладки | Общие настройки системы                                                           |
|    `~/.bashrc`     | Запуск нового терминала/вкладки | Настройки пользователя                                                            |

## Цепочки команд (конвейер)

### Статус выполнения

#### Успешный

```shell
user@myhost:~$ mkdir twice
```

#### Не успешный

```shell
user@myhost:~$ mkdir twice
user@myhost:~$ mkdir twice
mkdir: twice: File exists
```

#### `$?` Узнать статус выполнения предыдущей команды

```shell
user@myhost:~$ mkdir twice
user@myhost:~$ echo "$?"
0
user@myhost:~$ mkdir twice
mkdir: twice: File exists
user@myhost:~$ echo "$?"
1
user@myhost:~$
```

### Объединение команд

- `;` - Последовательное безусловное выполнение
- `&&` - IF - Выполнить следующую команду, если предыдущая успешная
- `||` - ELSE - Выполнить следующую команду, если предыдущаая провалилась
- `&`  - Выполнение в фононовом режиме
- `|`  - Передать выход первой команды на вход второй
- `{}` - Объединение команд
- `()` - Выставить приоритет
- `\` - Конкатенация

#### `;` Последовательное безусловное выполнение

```shell
mkdir dir
cd dir
```

```shell
mkdir dir; cd dir
```

#### `&&` IF - Выполнить следующую команду, если предыдущаая успешная

```shell
cp img*.jpg /media/Flash
# Если копирование на внешний носитель успешное, то удаляем исходные файлы
rm img*.jpg
```

```shell
cp img*.jpg /media/Flash && rm img*.jpg
```

#### `||` ELSE - Выполнить следующую команду, если предыдущаая провалилась

```shell
cd dir
# Если не удалось зайти в каталог - созданим его
mkdir dir
```

```shell
cd dir || mkdir dir
```

#### `&&` + `||` IF ELSE - тернарная операция

```
value !=0 ? number / value : throw new ArithmeticException("Value must not be zero");
```

```shell
ping -c 3 sberbnak.ru && echo "Success" || echo "Fail"
```

#### `&` Выполнение в фононовом режиме

```shell
java -jar app.jar &
```

#### `|` Передать выход первой команды на вход второй

```shell
ls -lah | less
```

#### `{}` Объединение команд

```shell
cd dir || { mkdir dir; cd dir; echo "Create dir" } && echo "Dir has existed"
```

#### `()` Выставить приоритет

```shell
(COMMAND_1 && COMMAND_2) || COMMAND_3
```

```shell
(ping -c 1 sberbank.ru && telnet sberbank.ru 433) || echo "Host does not work"
```

#### `\` Конкатенация

```shell
cp \
long/path/to/files/* \
another/long/path/to/files/
```

```shell
mkdir dir; \
cp file* dir && \
rm file*
```

## Перенаправление текста

- `>` `1>` - Перезаписать текст из stdout
- `>>` `1>>` - Дозаписать текст из stdout
- `>|` - Force перезаписть текст из stdout
- `2>` - Перезаписать текст из stderr
- `2>>` - Дозаписать текст из stderr
- `2>|` - Force перезаписть текст из stderr
- `&>` `>&` - Отправить stdout и stderr в один источник
- `&>>` `>>&` - Дозапись в stdout и stderr в один источник

### stdout

#### Перезапись

```shell
echo "Hello" > file.txt
```

```shell
ls -l > list_files.txt
```

```shell
echo "Hello" 1> file.txt
```

#### Дозапись

```shell
echo "Hello" >> file.txt
```

```shell
echo "Hello" 1>> file.txt
```

### stderr

#### Перезапись

```shell
ls % 2> file.txt
```

#### Дозапись

```shell
ls % 2>> file.txt
```

### stdout + stderr

#### Перезапись

```shell
echo "Hello" &> file.txt
```

```shell
echo "Hello" >& file.txt
```

#### Дозапись

```shell
echo "Hello" &>> file.txt
```

```shell
echo "Hello" >>& file.txt
```

### Перезапись файлов

```shell
user@myhost:~$ echo "Hello" > file.txt
user@myhost:~$ cat file.txt
Hello
user@myhost:~$ echo "World" > file.txt
user@myhost:~$ cat file.txt
World
```

```shell
user@myhost:~$ echo "1" > file.txt
user@myhost:~$ cat file.txt
1
user@myhost:~$ set -o noclobber
user@myhost:~$ echo "2" > file.txt
-bash: t.txt: cannot overwrite existing file
user@myhost:~$ cat file.txt
1
user@myhost:~$ echo "3" >| file.txt
user@myhost:~$ cat file.txt
3
user@myhost:~$ set +o noclobber
user@myhost:~$ echo "4" > file.txt
user@myhost:~$ cat file.txt
4
```

### stdin

```shell
user@myhost:~$ echo Hello world > file.txt
user@myhost:~$ cat file.txt
Hello world
user@myhost:~$ cat < file.txt
Hello world
```

### xargs

```shell
find path -type f | xargs tar -cvf arch.tar
```

#### Перенаправить файлы для удаления

```shell
user@myhost:~$ ls
file1 file2 file3
user@myhost:~$ find . -name 'file*'
./file1
./file2
./file3
user@myhost:~$ find . -name 'file*' | rm
usage: rm [-f | -i] [-dIPRrvWx] file ...
       unlink [--] file
user@myhost:~$ find . -name "testing.py" | xargs rm
user@myhost:~$ find . -name 'file*'
user@myhost:~$ ls
user@myhost:~$
```

*Данный листинг приводится в качестве примера. Команда `find` умеет удалять файлы самостоятельно*

## Символы групповых операций

|     Символы     | Аналог в Regex  | Пример          | Результат                  | Назначение                                  |
|:---------------:|-----------------|-----------------|----------------------------|---------------------------------------------|
|       `?`       | `.`             | ?.txt           | a.txt b.txt c.txt          | Заменить один любой символ                  |
|       `*`       | `.*`            | *.txt           | a.txt ab.txt abc.txt       | Заменить любое количество любых символов    |
|      `[]`       | `[]`            | img[1-3].jpg    | img1.jpg img2.jpg img3.jpg | Объединение                                 |
|      `{}`       | `[]`            | img{01..99}.jpg | img1.jpg img2.jpg img3.jpg | Объединение                                 |
| `[[:PATTERN:]]` | `[:PATTERN:]`   | [[:digit:]].txt | 1.txt                      | Специальные классы символов                 |

|    Шаблон    |                           Раскрытие                            | Объяснение                                                      |
|:------------:|:--------------------------------------------------------------:|-----------------------------------------------------------------|
| [[:alnum:]]  |                          [0-9A-Za-z]                           | Буквы и цифры                                                   |
| [[:alpha:]]  |                            [A-Za-z]                            | Буквы                                                           |
| [[:digit:]]  |                             [0-9]                              | Цифры                                                           |
| [[:lower:]]  |                             [a-z]                              | Буквы в нижнем регистре                                         |
| [[:upper:]]  |                             [A-Z]                              | Буквы в верхнем регистре                                        |
| [[:blank:]]  |                                                                | Пробел и табуляция                                              |
| [[:space:]]  |                               \s                               | Все пробельные символы (пробел, табуляция, новая строка и т.д.) |
| [[:punct:]]  | ! " # $ % & ' ( ) * + , - . / : ; < = > ? @ ^ _ ` { } ~ ] [ \\ | Специальные символы                                             |
| [[:xdigit:]] |                          [0-9A-Fa-f]                           | 16-чные цифры                                                   |
| [[:cntrl:]]  |                                                                | Управляющие символы                                             |
| [[:graph:]]  |                   [[:alnum:]] + [[:punct:]]                    | Все символы, которые можно написать без пробелов                |
| [[:print:]]  |            [[:alnum:]] + [[:punct:]] + [[:space:]]             | Все символы, которые можно написать                             |

### Пример

```shell
user@myhost:~$ touch a.txt b.txt c.txt ab.txt d1 d2 d3 file
user@myhost:~$ ls
a.txt	ab.txt	b.txt	c.txt	d1	d2	d3	file
user@myhost:~$ ls ?.txt
a.txt	b.txt	c.txt
user@myhost:~$ ls *.txt
a.txt	ab.txt	b.txt	c.txt
user@myhost:~$ ls d[1-3]
d1	d2	d3
user@myhost:~$ ls d[123]
d1	d2	d3
user@myhost:~$ ls d[13]
d1	d3
user@myhost:~$ ls d[[:digit:]]
d1	d2	d3
```

```shell
user@myhost:~$ touch img{01..11}.jpg
user@myhost:~$ ls
img01.jpg	img03.jpg	img05.jpg	img07.jpg	img09.jpg	img11.jpg
img02.jpg	img04.jpg	img06.jpg	img08.jpg	img10.jpg
```

```shell
user@myhost:~$ mkdir backup{1999,2022}-{01,05,09,10,11,12}-photo
user@myhost:~$ ls
backup1999-01-photo	backup1999-09-photo	backup1999-11-photo	backup2022-01-photo	backup2022-09-photo	backup2022-11-photo
backup1999-05-photo	backup1999-10-photo	backup1999-12-photo	backup2022-05-photo	backup2022-10-photo	backup2022-12-photo
```

```shell
ls *.{jpg,jpeg,png}
```

## Подстановки

|    Команда     |    Пример    | Объяснение                         |
|:--------------:|:------------:|------------------------------------|
|   $(COMMAND)   |   $(date)    | Выполнение команды Linux           |
|  \`COMMAND\`   |   \`date\`   | Выполнение команды Linux           |
|     ${ENV}     | ${JAVA_HOME} | Подставить переменную окружения    |
|      $ENV      | ${JAVA_HOME} | Подставить переменную окружения    |
| $((CALCULATE)) |   $((1+1))   | Выполнение арифметической операции |

### Команда

```shell
user@myhost:~$ echo "Today: $(date)"
Today: вторник,  8 ноября 2022 г. 20:12:17 (MSK)
```

```shell
user@myhost:~$ echo "Today: `date`"
Today: вторник,  8 ноября 2022 г. 20:12:17 (MSK)
```

### Переменная окружения

```shell
user@myhost:~$ echo "My shell: ${SHELL}"
My shell: /bin/bash
```

```shell
user@myhost:~$ echo Hello $USER
Hello user
```

### Арифметические операции

```shell
user@myhost:~$ echo $((1+1))
2
```

## Экранирование

| Символ |             Пример              | Объяснение                                                                           |
|:------:|:-------------------------------:|--------------------------------------------------------------------------------------|
|   \\   |        ls Новая\\ папка         | Превращает следующий за \\ символом из служебного в печатный                         |
|   \"   |    echo "It's text: ~/*.txt"    | Превращает символы групповых операций в печатные, но позволяет выполнять подстановки |
|   \'   | echo '$(( 5 * 8 )) = ' $((5*8)) | Превращает все служебные символы в печатные                                          |

### Пример

```shell
user@myhost:~$ echo text ~/*.txt {a,b} $(echo foo) $((2+2)) $USER
text /home/user/file.txt a b foo 4 user
```

```shell
user@myhost:~$ echo "text ~/*.txt {a,b} $(echo foo) $((2+2)) $USER"
text ~/*.txt {a,b} foo 4 user
```

```shell
user@myhost:~$ echo 'text ~/*.txt {a,b} $(echo foo) $((2+2)) $USER'
text ~/*.txt {a,b} $(echo foo) $((2+2)) $USER
```

```shell
user@myhost:~$ echo \$\(echo foo\)
$(echo foo)
user@myhost:~$ echo \$PATH
$PATH
```