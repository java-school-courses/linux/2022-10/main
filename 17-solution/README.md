# bash-скрипты

## bash-скрипт - это набор команд

bash-скрипт - это текстовый файл, в котором вы записали команды Linux, как будто ввели самостоятельно.\
Все предыдущие занятия обучали вас, как писать bash-скрипты.\
Осталось поставить финальную точку.

## Запуск

### Флаг `execute`

Чтобы `shell` мог запустить скрипт, у него должен присутствовать флаг `execute`

```shell
user@myhost:~$ cat script.sh 
#!/bin/bash
echo "My first script"

user@myhost:~$ ls -l script.sh
-rw-rw-r-- 1 user user 35 Nov 19 13:50 script.sh
user@myhost:~$ ./script.sh
-bash: ./script.sh: Permission denied

user@myhost:~$ chmod +x script.sh 
user@myhost:~$ ls -l script.sh
-rwxrwxr-x 1 user user 35 Nov 19 13:50 script.sh
user@myhost:~$ ./script.sh
My first script
```

### Shebang

Вторая обязательная деталь - `shebang`\
Так называется последовательность символов `#!`, за которой следует абсолютный путь к приложению.\
Он нужен, чтобы указать `shell` через какое приложение запускать скрипт\
Чаще всего указывается сам `shell`:

```shell
#!/bin/bash
```

Но это не обязательно\
Можно указать любое приложение, например Python

```shell
user@myhost:~$ cat python.sh 
#!/usr/bin/python3
print("This is Python")
user@myhost:~$ ./python.sh 
This is Python
```

#### "Плавающий" путь к интерпретатору

Если приложение, под которым вы хотите запустить скрипт не имеет стандартного пути,\
Например, Python может быть установлен в `/usr/bin/python`, `/usr/local/bin/python` и даже `/home/username/bin/python`\
То можно воспользоваться командой

```shell
#!/usr/bin/env python
```

Чтобы `shell` сам определил местоположение интерпретатора

### Любой интерпретатор

В shebang вы можете написать путь к любому приложению\
Запустится приложение, указанное в shebang, а ваш скрипт будет считаться первым аргументом командной строки\
Вы даже, можете передавать параметры

```shell
user@myhost:~$ cat cat.sh 
#!/usr/bin/cat
Hello, Cat
user@myhost:~$ ./cat.sh 
#!/usr/bin/cat
Hello, Cat
```

```shell
user@myhost:~$ cat verbose.sh 
#!/bin/bash --verbose
echo "This is bash with verbose"
user@myhost:~$ ./verbose.sh 
#!/bin/bash --verbose
echo "This is bash with verbose"
This is bash with verbose
```

#### Можно обойтись без shebang

Если запускать скрипт как аргумент другого `shell`, то можно обойтись без shebang\
Об этом способе ниже

```shell
user@myhost:~$ cat text.sh 
echo "This is plain text"
user@myhost:~$ /bin/bash text.sh 
This is plain text
```

### Способы запуска скрипта

Есть 3 способа запустить скрипт:

1. Указать абсолютный путь к скрипту
2. Указать имя скрипта, при этом вы можете находиться в любой директории
3. Запустить дочерний `shell` и передать в качестве аргумента скрипт

#### Запуск скрипта по абсолютному пути

Это самый простой и предпочтительный способ\
Вы передаете в `shell` абсолютный адрес, по которому лежит скрипт и `shell` его запускает

При этом, можно пользоваться сокращенными путями, при помощи символов `~`, `.`, `..`, `~USER`

```shell
/home/user/script.sh
```

```shell
~/script.sh
```

```shell
./script.sh
```

#### Запуск скрипта по его названию из любой директории

Если `shell` видит название команды, он пытается найти её в одной из директорий переменной окружения `PATH`

```shell
user@myhost:~$ cat my-app 
#!/bin/bash
echo "This is my application"
user@myhost:~$ my-app
my-app: command not found
user@myhost:~$ PATH="${PATH}:$(pwd)"
user@myhost:~$ my-app
This is my application
```

```shell
user@myhost:~$ cat ls
#!/bin/bash
echo "It is 'ls'"
user@myhost:~$ ls
ls
user@myhost:~$ ./ls
It is 'ls'
```

#### Передать аргумент другому `shell`

Этот способ ничем не отличается, от запуска любой другой команды с вашим скриптом в качестве аргумента.\
Стоит обратить внимание только на пару моментов:

- Такой запуск игнорирует `shebang`
- В `shell` есть команда `source`, которая запускает скрипты в текущем интерпретаторе, а не дочернем

### Расширение файла

Изначально в Unix, когда появился `shell`, не было концепции расширения файла.\
Поэтому скрипт может иметь любой текст после символа `.` в названии\
На логику запуска это никак не повлияет.

С другой стороны, чтобы другие пользователи системы понимали, с чем они работают принято указывать в расширении `.sh`\
Но это не обязательно и многие создатели скриптов просто не указывают расширения

## Комментарии

Комментарий начинается с символа `#` и идёт до конца строки

```shell
# Это комментарий
```

## Переменные

Переменные в bash-скриптах это те же переменные окружения, которые мы уже рассмотрели\
На них действуют те же правила

```shell
#!/bin/bash
my_var=Hello
echo "My var: ${my_var}"
```

## Входные аргументы

Вы можете передавать в скрипт входные аргументы, они будут доступны как переменные `$0`, `$1`, `$2` и т.д.\
`$0` - абсолютный путь к скрипту\
`$1` - первый входной аргумент

Переменная `$#` хранит число входных аргументов

```shell
user@myhost:~$ cat arguments.sh 
#!/bin/bash
echo "Count arguments: $#"
echo "\$0 = $0"
echo "\$1 = $1"
echo "\$2 = $2"
user@myhost:~$ ./arguments.sh First Second
Count arguments: 2
$0 = ./arguments.sh
$1 = First
$2 = Second
```

## Управляющие конструкции

### if

```shell
if CONDITION_COMMANDS...
then
    COMMANDS...
fi
```

`CONDITION_COMMANDS` - это обычные Linux команды.\
Напоминаю, что каждая команда в Linux возвращает статус: 0 - успешно/true, 1 - 255 - не успешно/false.\
По этому статусу определяется ветвление.\
Причём, таких команд может быть больше одной.\
Результат берётся по последней.

```shell
user@myhost:~$ cat if-v1.sh 
#!/bin/bash
if echo "Hello"
then
    echo "echo finish OK"
fi
user@myhost:~$ cat if-v1.sh
Hello
echo finish OK
```

```shell
user@myhost:~$ cat if-v2.sh 
#!/bin/bash
if echo "One";echo "Two"; then
    echo "echo finish OK"
fi
user@myhost:~$ ./if-v2.sh 
One
Two
echo finish OK
```

```shell
user@myhost:~$ cat if-v3.sh 
#!/bin/bash
if echo "Hello" >> /dev/full
then
    echo "This line will not appear"
fi
echo "Script finished"
user@myhost:~$ ./if-v3.sh 
./if-v3.sh: line 2: echo: write error: No space left on device
Script finished
```

#### if else

```shell
if CONDITION_COMMANDS...; then
    COMMANDS...
elif CONDITION_COMMANDS...; then
    COMMANDS...
else
    COMMANDS...
fi
```

```shell
user@myhost:~$ cat if-v4.sh 
#!/bin/bash
if echo "Hello" >> /dev/full
then
    echo "If (true) branch"
else
    echo "Else (false) branch"
fi
user@myhost:~$ ./if-v4.sh 
./if-v4.sh: line 2: echo: write error: No space left on device
Else (false) branch
```

#### Команда `test`

Работать с обычными командами в условиях if-else немного неудобно.\
Чтобы привести эти конструкции к привычному виду обычных условий в Linux существует команда `test`.\
Она возвращает:

- 0 (success), если условие верное
- 1 (fail), если условие не верное

Команду `test` можно записать 4-мя разными способами:

1. Обычная команда `test`, ей пользуются реже всего

```shell
test CONDITION
```

2. Старая сокращенная запись. Она больше распространена, но менее удобна.

```shell
[ CONDITION ]
```

3. Новая сокращенная запись. Более удобный и предпочтительный вариант.

```shell
[[ CONDITION ]]
```

4. Новая сокращенная запись для арифметических операций. 0 - fail, всё что не 0 - success.

```shell
(( CONDITION ))
```

##### Пример

```shell
#!/bin/bash
login="user01"
if test $login == "user01"
then
    echo "This is the one we need"
fi
```

```shell
#!/bin/bash
login="user01"
if [ $login == "user01" ]
then
    echo "This is the one we need"
fi
```

```shell
#!/bin/bash
login="user01"
if [[ $login =~ user[0-9]+ ]]
then
    echo "This is the one we need"
fi
```

### Циклы

#### while

```shell
while CONDITION_COMMANDS...
do
    COMMANDS...
done
```

```shell
#!/bin/bash
i = 1
while (( i < 5 ))
do
    echo $i
    i = $((i+1))
done
```

#### until

```shell
#!/bin/bash
i = 1
until [[ $i -gt 5 ]]
do
    echo $i
    i = $((i+1))
done
```

#### for

```shell
#!/bin/bash
for (( i = 0; i < 5; i++ ))
do
    echo $i
done
```

#### foreach

```shell
for NAME in RANGE
do
    COMMANDS...
done
```

```shell
#!/bin/bash
for i in {1..5}
do
    echo $i
done
```

```shell
#!/bin/bash
for file in $( ls )
do
    echo $file
done
```

### case

```shell
case VARIABLE in
  PATTERN1) COMMANDS... ;;
  PATTERN2 | PATTERN3) COMMANDS... ;;
esac
```

```shell
#!/bin/bash
case $(whoami) in
root )  echo "You have permission" ;;
user??) echo "You do not have permission" ;;
*)      echo "I do not know you" ;;
esac
```

```shell
#!/bin/bash
case $1 in
-h | --help ) 
  echo "Usage: myapp [options]"
  echo "    Options: -h, -v"  
  ;;
-v | --version) echo "1.0" ;;
esac
```

### Использование методов

```shell
function NAME() {
    COMMANDS...
}
```

```shell
NAME() {
    COMMANDS...
}
```

```shell
#!/bin/bash
hello() {
  echo "Hello, $1"
}

hello "World"
```

## Пример приложения

### [Программа перевода текста в верхний или нижний регистр](example/case.sh)

## Антипаттерн `Золотой молоток`

```text
Я думаю, что если твоим единственным инструментом является молоток, то на что угодно хочется смотреть как на гвоздь.
(c) Абрахам Маслоу
```

bash-скрипты имеют свои плюсы, в первую очередь связанные с богатыми возможностями `shell`.\
Но вести разработку bash-скриптов в классическом императивном виде неудобно.

Часто, люди, которые освоили Linux и `shell` стремятся автоматизировать рутину исключительно bash-скриптами.\
Это очень похоже на Антипаттерн `Золотой молоток`.

Если вам нужен инструмент, для написания скриптов, то присмотритесь к:

- [Python](https://www.python.org) - самый распространенный интерпретируемый язык программирования
- [Ansible](https://www.ansible.com) - специальный инструмент, написанный на Python, для автоматизации административной
  рутины
- [Ruby](https://www.ruby-lang.org/ru/) - неплохая альтернатива Python
- [GoLang](https://go.dev/tour/welcome/1) - современный компилируемый язык программирования.\
  Прост в написании и чтении так же, как Python. При этом приложения потребляют меньше памяти и выполняются гораздо
  быстрей.