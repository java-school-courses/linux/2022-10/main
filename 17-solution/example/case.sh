#!/bin/bash
# Пример приложения
# Переводит текст в верхний или нижний регистр


# Подпрограмма - справка о приложении
function help() {
    echo "Usage: case.sh options file"
    echo "Translate characters to uppercase or lowercase"
    echo "    Options:"
    echo "    -u, --up              Translate characters to uppercase"
    echo "    -d, --down, --lower   Translate characters to lowercase"
    echo "    -h, --help            Display help information"
    echo "    -v, --version         Display version information"
    echo
    echo "    Example:"
    echo "    ./case.sh --down file.txt"
    echo "    ls -l | ./case.sh --up"
}

# Проверка входных аргументов
# Их должно быть либо 1, либо 2
if (("$#" < 1 || 2 < "$#"))
then
  printf "Wrong number of input arguments: %d. Two needed.\n\n" "$#"
  help
  exit 1
fi

# Дерево решений
# Выбираем разные цепочки действий
# В зависимости от входных аргументов
case "$1" in
-h | --help )
    help ;;
-v | --version)
    echo "case.sh - Translate characters to uppercase or lowercase"
    echo "Version: 1.0"
    ;;
-u | --up)
    tr "a-z" "A-Z" $2
    ;;
-d | --down | --lower)
    tr "A-Z" "a-z" $2
    ;;
*)
    printf "Wrong option '%s'.\n" "$1"
    help
    ;;
esac