# Планировщик задач

В этом занятии мы научимся планировать задачи с помощью cron.\
В инструкциях [Планировщик задач](../19-solution/README.md) и [Работа с текстом](../13-solution/README.md) представлены
шпаргалки по командам.\
Все задания выполняются на удалённом сервере\
Доступ к которому мы отработали в первом занятии [Подключение к удаленному серверу](../01-exercise/README.md)

## Задание

1. Подключитесь под своим пользователем на удалённый сервер
2. Зайдите в редактор задач `cron`
3. Создайте задачу, которая раз в минуту будет добавлять строку `tik-tak` в файл `~/cron.txt`
4. Посмотрите, какие задачи запланированы от вашего пользователя
5. Откройте файл `~/cron.txt` так, чтобы добавление новых строк в файл автомотически отображалось в консоли
6. Подождите несколько минут и убедитесь, что в файл добавляются новые строки
7. Удалите запланированную задачу
8. Посмотрите, какие задачи запланированы от вашего пользователя - убедитесь, что запланированных задач больше не
   осталось