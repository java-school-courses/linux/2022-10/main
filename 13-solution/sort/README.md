# Сортировка

```shell
sort FILES...
```

## Обычная сортировка

По-умолчанию происходит лексикографическая сортировка

```shell
user@myhost:~$ cat text.txt
ccc
bbb
abc
cba
user@myhost:~$ sort text.txt
abc
bbb
cba
ccc
```

## Сортировка в обратном порядке

```shell
sort --reverse FILES...
```

```shell
sort -r FILES...
```

```shell
user@myhost:~$ cat text.txt
ccc
bbb
abc
cba
user@myhost:~$ sort -r text.txt
ccc
cba
bbb
abc
```

## Перетасовать

```shell
sort --sort=random FILES...
```

```shell
sort --random-sort FILES...
```

```shell
sort -R FILES...
```

```shell
user@myhost:~$ cat text.txt 
ccc
cba
bbb
abc
user@myhost:~$ sort -R text.txt
cba
abc
ccc
bbb
user@myhost:~$ sort -R text.txt
abc
bbb
ccc
cba
user@myhost:~$ sort -R text.txt
bbb
cba
abc
ccc
```

## Сортировка чисел

```shell
sort --sort=numeric FILES...
```

```shell
sort --numeric-sort FILES...
```

```shell
sort -n FILES...
```

```shell
user@myhost:~$ cat numbers.txt
22
1
11
200
2
101
user@myhost:~$ sort numbers.txt 
1
101
11
2
200
22
user@myhost:~$ sort -n numbers.txt
1
2
11
22
101
200
```

## Сортировка human величин памяти

```shell
sort --sort=human-numeric FILES...
```

```shell
sort --human-numeric-sort FILES...
```

```shell
sort -h FILES...
```

```shell
user@myhost:~$ cat human-size.txt
3,5G
11M
200B
user@myhost:~$ sort human-size.txt
11M
200B
3,5G
user@myhost:~$ sort -n human-size.txt
3,5G
11M
200B
user@myhost:~$ sort -h human-size.txt
200B
11M
3,5G
```

## Сортировка столбцов таблицы

```shell
sort +POSITION FILES...
```

```shell
sort -POSITION FILES...
```

```shell
sort --key=FIELD FILES...
```

```shell
sort -k FIELD FILES...
```

### Сортировка по 2-му столбцу

```shell
user@myhost:~$ cat table.txt
1   ccc
2   cba
3   bbb
4   abc
user@myhost:~$ sort +1 table.txt
4   abc
3   bbb
2   cba
1   ccc
user@myhost:~$ sort -k 2 table.txt
4   abc
3   bbb
2   cba
1   ccc
```

### Произвольный разделитель таблицы

```shell
sort +POSITION --field-separator=CHAR FILES...
```

```shell
sort +POSITION -t CHAR FILES...
```

```shell
user@myhost:~$ cat users.txt
bbb:x:1003:1003:Regular user:/home/user:/bin/bash
cba:x:1002:1002:Regular user:/home/user:/bin/bash
abc:x:1004:1004:Regular user:/home/user:/bin/bash
ccc:x:1001:1001:Regular user:/home/user:/bin/bash
user@myhost:~$ sort +2 -t ':' users.txt
ccc:x:1001:1001:Regular user:/home/user:/bin/bash
cba:x:1002:1002:Regular user:/home/user:/bin/bash
bbb:x:1003:1003:Regular user:/home/user:/bin/bash
abc:x:1004:1004:Regular user:/home/user:/bin/bash
```

## Убрать дубликаты

```shell
sort --unique FILES...
```

```shell
sort -u FILES...
```

```shell
user@myhost:~$ cat repeat.txt
ccc
ccc
bbb
bbb
abc
abc
cba
cba
user@myhost:~$ sort repeat.txt 
abc
abc
bbb
bbb
cba
cba
ccc
ccc
user@myhost:~$ sort -u repeat.txt
abc
bbb
cba
ccc
```

## Проверить, что файл отсортирован

```shell
sort --check FILES...
```

```shell
sort -c FILES...
```

```shell
user@myhost:~$ cat text.txt
ccc
bbb
abc
cba
user@myhost:~$ sort -c text.txt
sort: text.txt:2: disorder: cba
```

```shell
user@myhost:~$ cat sort.txt
abc
bbb
cba
ccc
user@myhost:~$ sort -c sort.txt
```