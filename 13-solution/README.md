# Работа с текстом

## Текстовый редактор

[Текстовый редактор](editor/README.md)

## Отображение

### Простой вывод

```shell
echo TEXT
```

```shell
echo Hello world
```

```shell
echo "Hello world"
```

### Форматированный вывод

```shell
user@myhost:~$ printf "File %s does not exist\nYou have %d attempts left" "file.txt" 3
File file.txt does not exist
You have 3 attempts left
```

### Вывести номера строк

```shell
nl FILE
```

```shell
nl file.txt
```

#### Задать шаг номеров строк

```shell
nl -i 10 file.txt
```

#### Задать первый номер строки

```shell
nl -v 10 file.txt
```

#### Добавить разделитель между текстом и номером

```shell
nl -s " - " file.txt
```

## Просмотр

### Показать N первых строк

```shell
head FILES...
```

#### Показать первые 10 строк

```shell
head file.txt
```

#### Задать количество выводимых строк

```shell
head --lines=5 file.txt
```

```shell
head -n 5 file.txt
```

#### Задать количество выводимых байт

```shell
head --bytes=50 file.txt
```

```shell
head -c 50 file.txt
```

### Показать N последних строк

```shell
tail FILES...
```

#### Показать последние 10 строк

```shell
tail file.txt
```

#### Отображение новых логов в реальном времени

```shell
tail -f file.txt
```

#### Задать количество выводимых строк

```shell
tail --lines=5 file.txt
```

```shell
tail -n 5 file.txt
```

#### Вывести весь текст, начиная с 10-й строки и до конца файла

```shell
tail --lines +10 file.txt
```

```shell
tail -n +10 file.txt
```

#### Задать количество выводимых байт

```shell
tail --bytes=50 file.txt
```

```shell
tail -c 50 file.txt
```

### Просмотр файла

```shell
less FILE
```

```shell
less file.txt
```

### Управляющие команды

Приложение `less` использует те же команды навигации и управления, как в приложении `vi`

- `q` - Выход
- `Вниз` - пролистать вниз
- `Вверх` - пролистать вверх

## Создание и вывод

### С помощью `cat`

```shell
cat > file.txt
```

### Записать и тут же вывести

```shell
tee FILES...
```

```shell
tee file.txt
```

## Поиск

```shell
grep PATTERN FILES...
```

### Пример

```shell
grep 'substring' file.txt
```

```shell
grep substring file.txt
```

```shell
ls -l | grep '-rw-r--r--'
```

### Поиск по регулярному выражению

```shell
grep '\d\d:\d\d' file.txt
```

```shell
grep -E '\d{2}:\d{2}' file.txt
```

### Вывести номер строки

```shell
grep 'substring' -n file.txt
```

### Вывести только количество найденных строк

```shell
grep 'substring' -c file.txt
```

### Игнорировать регистр

```shell
grep 'SubString' -i file.txt
```

### Логическое ИЛИ

```shell
grep -e 'first' -e 'second' file.txt
```

### Вывести все строки, которые не содержат образец

```shell
grep 'substring' -v file.txt
```

### Ищет выражение как слово, как если бы оно было окружено метасимволами \< и \>.

```shell
grep 'substring' -v file.txt
```

### Альтернативы `grep`

- `sed`
- `awk`
- `perl`

## Сортировка

### [Сортировка](sort/README.md)

## Обрезка текста справа и слева

### Обрезка таблицы

По-умолчанию символ разделения - табуляция

```shell
cut -f LIST FILES...
```

```shell
user@myhost:~$ cat table.txt       
1	ccc	6,5K
2	cba	200B
3	bbb	41M
4	abc	1G
user@myhost:~$ cut -f 2 table.txt
ccc
cba
bbb
abc
user@myhost:~$ cut -f "2 3" table.txt
ccc 6,5K
cba	200B
bbb	41M
abc	1G
```

#### Произвольный символ разделения

```shell
cut -f LIST -d CHAR FILES...
```

```shell
user@myhost:~$ cat users.txt
bbb:x:1003:1003:Regular user:/home/user:/bin/bash
cba:x:1002:1002:Regular user:/home/user:/bin/bash
abc:x:1004:1004:Regular user:/home/user:/bin/bash
ccc:x:1001:1001:Regular user:/home/user:/bin/bash
user@myhost:~$ cut -f "1 3" -d ':' users.txt
bbb:1003
cba:1002
abc:1004
ccc:1001
```

### Посимвольная обрезка

```shell
cut -с LIST FILES...
```

```shell
user@myhost:~$ cat users2.txt
user101:x:1003:1003:Regular user:/home/user:/bin/bash
user1:x:1002:1002:Regular user:/home/user:/bin/bash
user@myhost:~$ cut -c "1-8" user2.txt
user101:
user1:x:
```

## Посчитать строки, слова, символы в тексте

```shell
wc FILES...
```

```shell
user@myhost:~$ wc file.txt
       4      12      41 file.txt
      [-]    [--]    [--]
       |       |      |
       |       |      +--------> Байты
       |       +---------------> Слова
       +-----------------------> Строки
```

### Показать число строк в файле

```shell
wc -l file.txt
```

### Показать число слов в файле

```shell
wc -w file.txt
```

### Показать число символов в файле

```shell
wc -m file.txt
```

### Показать число байт в файле

```shell
wc -c file.txt
```

## Трансформация текста

```shell
tr PATTERN_IN PATTERN_OUT
```

### Преобразовать текст в верхний регистр

```shell
tr "a-z" "A-Z" < file.txt 
```

## Сравнение двух файлов

```shell
diff FILE1 FILE2
```

```shell
diff new-file.txt backup-file.txt
```

## Текстовые утилиты и конвейер

Команды работы с конвейером и перенаправлением передают данные в текстовом виде.\
Поэтому, чаще всего, чаще всего в таких операциях участвуют текстовые утилиты.\
Приведём небольшие примеры.\
Единственное ограничение здесь - ваша фантазия.

### Просмотр длинного вывода

```shell
ls -l /var/log/ | less
```

```shell
ps -a | less
```

### Поиск полезной информации

```shell
ps -a | grep java
```

```shell
netstat -l | grep 8080
```

```shell
env | grep PATH
```

```shell
grep "12:00" /var/log/message | grep "error"
```